import * as log from 'std/log/mod.ts';
import { getLevelName } from 'std/log/levels.ts';

import * as scm from 'libs/scm/mod.ts';

const LEVELS = [
    log.LogLevels.DEBUG,
    log.LogLevels.INFO,
    log.LogLevels.WARNING,
    log.LogLevels.ERROR,
    log.LogLevels.CRITICAL,
] as const;

class Logger extends log.Logger {
    get async() {
        return Object.fromEntries(LEVELS.map((level) => [
            getLevelName(level).toLowerCase() as Lowercase<Exclude<log.LevelName, 'NOTSET'>>,
            <T>(msg: () => Promise<T>, ...args: unknown[]) => {
                return this.#log(level, msg, ...args);
            },
        ]));
    }

    // @ts-ignore: hack
    constructor(logger: log.Logger) {
        return logger as any;
    }

    async #log<T>(level: log.LogLevels, msg: () => Promise<T>, ...args: unknown[]) {
        if (this.level <= level) {
            const fnName = getLevelName(level).toLowerCase() as Lowercase<Exclude<log.LevelName, 'NOTSET'>>;
            this[fnName].bind(this)(await msg() as any, ...args);
        }
    }
}

const CACHE = new Map<string, Logger>();

export function getLogger(name?: string) {
    name ??= 'default';
    let logger = CACHE.get(name);
    if (logger) {
        return logger;
    }
    logger = new Logger(log.getLogger(name));
    CACHE.set(name, logger);
    return logger;
}

export function setup(level: log.LogLevels) {
    const levelName = getLevelName(level);
    log.setup({
        handlers: {
            console: new log.handlers.ConsoleHandler(levelName, {
                formatter: ({ datetime, levelName, msg }: log.LogRecord) => {
                    return `${datetime.toISOString()} [${levelName}]${msg && ` ${msg}`}`;
                },
            }),
        },
        loggers: {
            default: {
                level: levelName,
                handlers: ['console'],
            },
            [scm.LOGGER]: {
                level: levelName,
                handlers: ['console'],
            },
        },
    });
}
