import { isSuccessfulStatus } from 'std/http/mod.ts';

import type { Commit } from 'libs/scm/mod.ts';

import { Config } from './config.ts';
import { createApi } from './utils.ts';

const CACHES = new Map<string, CLAClient>();

// TODO: use redis to cache cla info
export class CLAClient {
    #api;
    #cache: Set<string> | null = null;

    constructor(community: string) {
        const instance = CACHES.get(community);
        if (instance) {
            return instance;
        }
        const { cla: { api: url, id } } = new Config().communities.get(community)!;
        const api = createApi(`${url}/individual-signing/${id}`, { method: 'GET' });
        this.#api = {
            check: api`?email=${'email'}`,
        };
        CACHES.set(community, this);
    }

    async check(commits: Commit[], force = false) {
        if (!this.#cache) {
            force = true;
            this.#cache = new Set();
        }
        const emails = [...new Set(commits.map(({ author: { email } }) => email))];
        if (force) {
            await Promise.all(emails.map(async (email) => {
                const resp = await this.#api!.check({ params: { email } });
                if (isSuccessfulStatus(resp.status) && (await resp.json())['data']['signed']) {
                    this.#cache!.add(email);
                }
            }));
        }
        return commits.filter(({ author: { email } }) => !this.#cache!.has(email));
    }
}
