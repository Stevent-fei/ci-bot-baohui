import * as base64 from 'std/encoding/base64.ts';
import { readAll } from 'std/streams/mod.ts';

const data = JSON.parse(await Deno.readTextFile('key.json'));
const publicKey = await crypto.subtle.importKey(
    'spki',
    base64.decode(data.publicKey),
    { name: 'RSA-OAEP', hash: 'SHA-256' },
    false,
    ['encrypt'],
);
const privateKey = await crypto.subtle.importKey(
    'pkcs8',
    base64.decode(data.privateKey),
    { name: 'RSA-OAEP', hash: 'SHA-256' },
    false,
    ['decrypt'],
);

if ('encrypt' === Deno.args[0]) {
    console.log(base64.encode(await crypto.subtle.encrypt('RSA-OAEP', publicKey, await readAll(Deno.stdin))));
} else if ('decrypt' === Deno.args[0]) {
    const decoder = new TextDecoder();
    console.log(
        decoder.decode(
            await crypto.subtle.decrypt(
                'RSA-OAEP',
                privateKey,
                base64.decode(decoder.decode(await readAll(Deno.stdin))),
            ),
        ),
    );
} else {
    Deno.exit(1);
}
