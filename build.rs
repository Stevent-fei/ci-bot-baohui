use std::env;
use std::fs;
use std::path::Path;

fn main() {
    let data = jsonc_parser::parse_to_serde_value(include_str!("deno.jsonc"), &Default::default())
        .unwrap()
        .unwrap();
    let cmdline = data["tasks"]["start"].to_string();
    let perms = cmdline
        .split(" ")
        .filter(|&x| x.starts_with("--allow-"))
        .map(|x| format!("{}: Some(vec![]),", x[2..].replace("-", "_")))
        .collect::<Vec<String>>()
        .join("\n");
    let out_dir = env::var_os("OUT_DIR").unwrap();
    fs::write(
        Path::new(&out_dir).join("perms_opts.rs"),
        format!(
            "PermissionsOptions {{
                {perms}
                allow_hrtime: true,
                prompt: false,
                ..Default::default()
            }}
            "
        ),
    )
    .unwrap();
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=deno.json");
}
